package com.jqlin.mybatis.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import com.jqlin.mybatis.domain.User;

@Controller
//@RequestMapping("/usermv") //do会默认加上
public class UserActionMV  {
	
	@RequestMapping(value="modelView")
	public ModelAndView modelView(String userName) {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("jsp/mv");;
		
		mv.addObject("msg", "helloKitty");
		
		List<String> l1 = new ArrayList<String>();
		l1.add("l1");
		l1.add("l2");
		l1.add("l3");
		mv.addObject("list",l1);
		
		Map<String, String> m = new HashMap<String, String>();
		m.put("m1", "bj");
		m.put("m2", "sh");
		m.put("m3", "gz");
		mv.addObject("map", m);
		
		User u = new User();
		u.setUserName("haha");
		mv.addObject("u", u);
		
		return mv;
	}
}
