package com.jqlin.mybatis.dao;

import java.io.Serializable;

import com.jqlin.mybatis.domain.User;

/**
 * mybatis baseDao使用
 * @author linjq
 *
 */
public interface UsersDao extends BaseDao<User>{

}
