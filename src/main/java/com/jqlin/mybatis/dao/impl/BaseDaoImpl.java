package com.jqlin.mybatis.dao.impl;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;

import com.jqlin.mybatis.dao.BaseDao;
import com.jqlin.mybatis.util.PageControl;

public class BaseDaoImpl<T> implements BaseDao<T>{
	@Resource(name="sqlSessionTemplate")
    private SqlSession session;
	private final String path = "com.jqlin.mybatis.mapper.";
	private Class type;
    
	private String getMethodPath(String methodType) {
        return path + type.getSimpleName() + "Mapper." + methodType;
    }
    
    private Class getDAOClass(){
         Class clazz = (Class)((ParameterizedType) this.getClass().getGenericSuperclass())
            .getActualTypeArguments()[0];
         return clazz;
    }
    
    
    public BaseDaoImpl() {
		this.type = getDAOClass();
	}
    
	@Override
	public void save(T obj) {
		session.insert(getMethodPath("save"), obj);
	}

	@Override
	public void delete(T obj) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(T obj) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public T get(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<T> list() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<T> list(PageControl<T> pn) {
		// TODO Auto-generated method stub
		return null;
	}

}
