package com.jqlin.mybatis.dao.manage;

import java.util.HashMap;
import java.util.List;

import com.jqlin.mybatis.domain.Article;
import com.jqlin.mybatis.domain.User;

/**
 * mapper使用方法
 * @author linjq
 */
public interface UserDao {
	public User selectUserByID(int id);
	public HashMap selectMapByid(int id);
	public List<HashMap> selectListMap();
	public int addUser(User user);
	public void updateUser(User user);
	public void deleteUser(int id);
	public  List<Article>  getUserArticles(int id);
	public List<User> selectUserByParam(User user);
	public void updateUserByParam(int id, String userName, String userAge,
			String userAdress);
}
