package com.jqlin.mybatis.dao.web;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Component;

import com.jqlin.mybatis.domain.Article;
import com.jqlin.mybatis.domain.User;

/**
 * mapper使用方法
 * @author linjq
 */
@Component(value="userDaoWeb")
public interface UserDao {
	public User selectUserByID(int id);
}
