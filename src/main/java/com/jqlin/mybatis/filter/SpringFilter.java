package com.jqlin.mybatis.filter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.jqlin.mybatis.log.LogUtil;

public class SpringFilter implements HandlerInterceptor {

	@Override
	public void afterCompletion(HttpServletRequest arg0,
			HttpServletResponse arg1, Object arg2, Exception arg3)
			throws Exception {
		// TODO Auto-generated method stub
		LogUtil.infoLogger(SpringFilter.class, "===>最后执行，通常用于释放资源，处理异常。我们可以根据ex是否为空，来进行相关的异常处理。");
	}

	@Override
	public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1,
			Object arg2, ModelAndView arg3) throws Exception {
		// TODO Auto-generated method stub
		LogUtil.infoLogger(SpringFilter.class, "===>该方法在action执行后，生成视图前执行。");
	}

	@Override
	public boolean preHandle(HttpServletRequest arg0, HttpServletResponse arg1,
			Object arg2) throws Exception {
		// TODO Auto-generated method stub
		LogUtil.infoLogger(SpringFilter.class, "===>action前执行");
		return true;
	}

}
