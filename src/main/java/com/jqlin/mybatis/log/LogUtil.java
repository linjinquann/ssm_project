package com.jqlin.mybatis.log;

import org.apache.log4j.Logger;

public class LogUtil {
	
	private static Logger getLogger(Class clazz) {
		return Logger.getLogger(clazz);
	}

	public static void infoLogger(Class clazz, String msg) {
		Logger log = getLogger(clazz);
		log.info(clazz.getSimpleName() + "：" + msg);
	}
}
