package com.jqlin.mybatis.service;

import java.util.HashMap;
import java.util.List;

import com.jqlin.mybatis.domain.Article;
import com.jqlin.mybatis.domain.User;

public interface UserService {
	public int saveUser(User user);

	public User selectUserByID(int id);

	public HashMap selectMapByid(int i);

	public List<HashMap> selectListMap();

	public void updateUser(User u);

	public void deleteUser(int i);

	public List<Article> getUserArticles(int i);

	public User savesUser(int id);

	public List<User> selectUserByParam(User user);

	public void updateUserByParam(int id, String userName, String userAge,
			String userAdress);

	public User getUserWebByID(int id);

}
