package com.jqlin.mybatis.service.impl;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jqlin.mybatis.dao.UsersDao;
import com.jqlin.mybatis.dao.UsersDao;
import com.jqlin.mybatis.dao.manage.UserDao;
import com.jqlin.mybatis.domain.Article;
import com.jqlin.mybatis.domain.User;
import com.jqlin.mybatis.service.UserService;


//@service标记业务层bean
@SuppressWarnings("unused")
@Service(value="userService")
public class UserServiceImpl  implements UserService {
	@Resource
	private UserDao userDao;
	@Resource
	private com.jqlin.mybatis.dao.web.UserDao userDaoWeb;
	@Resource
	private UsersDao usersDao;
	

	
	public com.jqlin.mybatis.dao.web.UserDao getUserDaoWeb() {
		return userDaoWeb;
	}

	@Resource
	public void setUserDaoWeb(com.jqlin.mybatis.dao.web.UserDao userDaoWeb) {
		this.userDaoWeb = userDaoWeb;
	}


	@Override
	public int saveUser(User user) {
		
		return this.userDao.addUser(user);
	}


	@Override
	public HashMap selectMapByid(int id) {
		// TODO Auto-generated method stub
		return this.userDao.selectMapByid(id);
	}


	@Override
	public List<HashMap> selectListMap() {
		// TODO Auto-generated method stub
		return this.userDao.selectListMap();
	}


	@Override
	public void updateUser(User u) {
		// TODO Auto-generated method stub
		this.userDao.updateUser(u);
	}


	@Override
	public void deleteUser(int id) {
		// TODO Auto-generated method stub
		this.userDao.deleteUser(id);
	}


	@Override
	public List<Article> getUserArticles(int id) {
		// TODO Auto-generated method stub
		return this.userDao.getUserArticles(id);
	}

	@Override
	public User savesUser(int id) {
		// TODO Auto-generated method stub
		User u = new User();
		u.setUserName("haha1");
		this.usersDao.save(u);
		return null;
	}


	@Override
	public User selectUserByID(int id) {
		// TODO Auto-generated method stub
		return this.userDao.selectUserByID(id);
	}


	@Override
	public List<User> selectUserByParam(User user) {
		// TODO Auto-generated method stub
		return this.userDao.selectUserByParam(user);
	}

	

	@Override
	public void updateUserByParam(int id, String userName, String userAge,
			String userAdress) {
		this.userDao.updateUserByParam(id, userName, userAge, userAdress);
		
	}


	@Override
	public User getUserWebByID(int id) {
		// TODO Auto-generated method stub
		return this.userDaoWeb.selectUserByID(id);
	}


}
