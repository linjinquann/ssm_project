package com.jqlin.mybatis;

import com.jqlin.mybatis.manage.MyBatisUserTest;

import junit.framework.TestSuite;

public class AllTest {
	public static TestSuite suite(){
		TestSuite ts = new TestSuite();
		ts.addTestSuite(MyBatisUserTest.class);
		return ts;
	}
}
