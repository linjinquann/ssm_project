package com.jqlin.mybatis.manage;

import java.io.IOException;
import java.io.Reader;
import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import com.jqlin.mybatis.dao.manage.UserDao;
import com.jqlin.mybatis.domain.Article;
import com.jqlin.mybatis.domain.User;

import junit.framework.TestCase;

/**
 * 测试mybatis api，不使用spring容器
 * @author Administrator
 *
 */
public class MyBatisUserTest extends TestCase	 {
	private Reader reader;
	private SqlSessionFactory sqlSessionFactory;
	private SqlSession session;
	UserDao userDao;
	
	
	//junit先运行时，还执行这方法
	@Override
	protected void setUp() throws Exception {
		reader = Resources.getResourceAsReader("mybatis_user_test.xml");
		sqlSessionFactory = new SqlSessionFactoryBuilder().build(reader);
		//opensession开启一个事务，不会自动提交，需要commit
		session = sqlSessionFactory.openSession();
		userDao = session.getMapper(UserDao.class);
		super.setUp();
	}



	//调用xml的方式
	public void testXML() throws IOException {
		
		User user = (User) session.selectOne("com.jqlin.mybatis.domain.UserMapper.selectUserByID", 1);
		assertEquals("summer", user.getUserName());
		session.close();
	}

	//2.1接口方式,返回对象
	public void testInterface() {
		
		User u = userDao.selectUserByID(1);
		assertEquals("summer", u.getUserName());
		session.close();
	}
	
	
	//2.2接口方式,返回hashMap
	public void testInterfaceMap() {
		
		HashMap u = userDao.selectMapByid(1);
		assertEquals("summer", u.get("userName"));
		session.close();
	}
	
	//2.3接口方式，返回List
	public  void testInterfaceList() {
		List<HashMap> list = userDao.selectListMap();
		assertEquals(2, list.size());
		session.close();
	}
	
	//3. addUser
	public void testAddUser() {
//		User u = new User();
//		u.setUserAddress("road 95");
//		u.setUserName("ha95");
//		u.setUserAge("95");
//		iUserMapper.addUser(u);
//		session.commit(); //不commit不会提交数据库
//		System.out.println(u.getId());
//		session.close();
	}
	
	public void testUpdateUser() {
		User u = new User();
		u.setId(5);
		u.setUserAddress("road 95");
		u.setUserName("ha97");
		u.setUserAge("95");
		userDao.updateUser(u);
		session.commit();
		session.close();
	}
	
	//delete
	public void testDelete() {
//		iUserMapper.deleteUser(7);
//		session.commit();
//		session.close();
	}
	
	
	public void testMoreToOne() {
		 List<Article> articles = userDao.getUserArticles(1);
         for(Article article:articles){
             System.out.println(article.getTitle()+":"+article.getContent()+
                     ":作者是:"+article.getUser().getUserName()+":地址:"+
                      article.getUser().getUserAddress());
         }
	}
	
}
