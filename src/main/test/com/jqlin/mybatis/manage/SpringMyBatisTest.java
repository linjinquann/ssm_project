package com.jqlin.mybatis.manage;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.jqlin.mybatis.domain.Article;
import com.jqlin.mybatis.domain.User;
import com.jqlin.mybatis.service.UserService;

import junit.framework.TestCase;

/**
 * 使用spring容器
 * @author Administrator
 *
 */
public class SpringMyBatisTest extends TestCase {
	private static ApplicationContext ctx;
	UserService userService;
	
	// junit先运行时，还执行这方法
	@Override
	protected void setUp() throws Exception {
		ctx = new ClassPathXmlApplicationContext("spring-mybatis.xml");
		userService = (UserService) ctx.getBean("userService");
		super.setUp();
	}


	// 2.2接口方式,返回hashMap
	public void testInterfaceMap() {
		HashMap u = userService.selectMapByid(1);
		assertEquals("summer", u.get("userName"));
	}

	// 2.3接口方式，返回List
	public void testInterfaceList() {
		List<HashMap> list = userService.selectListMap();
		assertEquals(2, list.size());
	}

	// 3. addUser
	public void testAddUser() {
//		 User u = new User();
//		 u.setUserName("ha92");
//		 userService.saveUser(u);
//		 System.out.println(u.getId());
	}

	public void testUpdateUser() {
		User u = new User();
		u.setId(5);
		u.setUserName("ha95");
		userService.updateUser(u);
	}

	// delete
	public void testDelete() {
		 userService.deleteUser(11);
	}
	
	/**
	 * 测试多对一
	 */
	public void testMoreToOne() {
		List<Article> articles = userService.getUserArticles(1);
		for (Article article : articles) {
			System.out.println(article.getTitle() + ":" + article.getContent()
					+ ":作者是:" + article.getUser().getUserName() + ":地址:"
					+ article.getUser().getUserAddress());
		}
	}
	
	/**
	 * 测试动态sql select
	 */
	public void testDynSql(){
		User user = new User();
		user.setUserAge("%9%");
		user.setUserName("%ha%");
		List<User> slist = this.userService.selectUserByParam(user);
		System.out.println(slist.size());
	}

	/**
	 * 动态sql update
	 */
	public void testDynSqlUpdate(){
		this.userService.updateUserByParam(12, null, "39", "road 39");
	}
}
