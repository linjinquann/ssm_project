package com.jqlin.mybatis.web;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.jqlin.mybatis.domain.Article;
import com.jqlin.mybatis.domain.User;
import com.jqlin.mybatis.service.UserService;

import junit.framework.TestCase;

/**
 * 使用spring
 * @author linjq
 *
 */
public class SpringMyBatisTest extends TestCase {
	private static ApplicationContext ctx;
	UserService userService;
	
	// junit先运行时，还执行这方法
	@Override
	protected void setUp() throws Exception {
		ctx = new ClassPathXmlApplicationContext("spring-mybatis.xml");
		userService = (UserService) ctx.getBean("userService");
		super.setUp();
	}


	// 2.2接口方式,返回hashMap
	public void testInterfaceMap() {
		User u = userService.getUserWebByID(1);
	}

	
}
