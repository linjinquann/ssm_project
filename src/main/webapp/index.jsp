<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script>
	function createAjaxObj() {
		var req;
		if (window.XMLHttpRequest) {
			req = new XMLHttpRequest();
		} else {
			req = new ActiveXObject("Msxml2.XMLHTTP"); //ie
		}
		return req;
	}

	function sendAjaxReq() {
		var req = createAjaxObj();
		req.open("get", "/user?method=myajax&userName=张三");
		req.setRequestHeader("accept", "application/json");
		req.onreadystatechange = function() {
			eval("var result=" + req.responseText);
			document.getElementById("json").innerHTML = result[0].userName;
		}
		req.send(null);
	}
	function sendLogin() {
		var req = createAjaxObj();
		req.open("get", "/user?method=login&id=1");
		req.setRequestHeader("accept", "application/json");
		req.onreadystatechange = function() {
			eval("var result=" + req.responseText);
			document.getElementById("login").innerHTML = result.status + "<br/>" 
																						+ result.user.userName;
		}
		req.send(null);
	}
	
	
</script>



</head>

<body>
	wellcome


	<div>
		=========servet_visit_start=========
		<!-- 以servlet访问controller为user, method=get时访问get, post访问post -->
		<form action="/user" method="get">
			<input type="text" name="username" /> <input type="submit"
				value="提交">
		</form>
	</div>
	<br />
	<br />
	<div>
		=========struts1_visit_start=========
		<form action="/user.do" method="get">
			<input type="hidden" name="method" value="add" /> <input type="text"
				name="username" /> <input type="submit" value="提交">
		</form>
	</div>
	<br />
	<br />
	<div>
		=========struts2_visit_start=========
		<form action="/user/likeStruts2" method="get">
			<input type="text" name="userName" /> <input type="submit"
				value="提交">
		</form>
	</div>
	<br />
	<br />
	<div>
		=========redirect_visit_start=========
		<!-- 以servlet访问controller为user, method=get时访问get, post访问post -->
		<form action="/user" method="get">
			<input type="hidden" name="method" value="redirectUrl" /> <input
				type="text" name="userName" /> <input type="submit" value="提交">
		</form>
	</div>
	<br />
	<br />
	<div>
		=========forward_visit_start=========
		<!-- 以servlet访问controller为user, method=get时访问get, post访问post -->
		<form action="/user" method="get">
			<input type="hidden" name="method" value="forwardUrl" /> <input
				type="text" name="userName" /> <input type="submit" value="提交">
		</form>
	</div>
	<br />
	<br />

	<div>
		=========modelAndView_visit_start=========
		<form action="/user/modelView" method="get">
			<input type="text" name="userName" /> <input type="submit"
				value="提交">
		</form>
	</div>
	<br />
	<br />

	<div>
		=========upload_visit_start=========
		<form action="/user/upload" method="post"
			enctype="multipart/form-data">
			<input type="text" name="userName" /> <input type="file" name="file" />
			<input type="submit" />
		</form>
	</div>
	<br />
	<br />

	<div>
		=========json_visit_start========= 
		<a href="javascript:void(0);" 	onclick="sendAjaxReq();">测试json</a>
		<div id="json"></div>
	</div>
	<br />
	<br />

	<div>
		=========mybatis_visit_start========= 
		<a href="javascript:void(0);" 	onclick="sendLogin();">测试json</a>
		<div id="login"></div>
	</div>
	<br />
	<br />

</body>
</html>